#include <iostream>
#include <bitset>
#include <vector>
#include <array>

std::array<char, 64> constexpr encode_table {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
    'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

std::string encode(std::string s);
std::string decode(std::string s);
int binstoi(std::string s);

int main(int argc, char* argv[]) {
    if(argc == 1) {
        std::cout << "b64 requires at least 2 arguments!";
        return -1; 
    }

    if(argc > 3) {
        std::cout << "b64 requires max 2 arguments!";
        return -1; 
    }

    std::string instruction = argv[1] == NULL ? "" : argv[1];
    std::string str_to_convert = argv[2] == NULL ? "" : argv[2];

    if(instruction != "" && str_to_convert != "") {
        if(instruction == "-e") {
            std::cout << encode(str_to_convert) << std::endl;
        } else if(instruction == "-d") {
            std::cout << decode(str_to_convert) << std::endl;
        } else {
            std::cout << "Invalid arguments \n";
        }
    } else {
        std::cout << "Invalid arguments \n";
    }


    return 0;
}

int binstoi(std::string s) {
    int total = 1;
    int result = 0;
    for(int i = s.length() - 1; i >= 0; i--) {
        if(s[i] == '1') {
           result += total;
        }

        total *= 2;
    }

    return result;
}

std::string encode(std::string s) {
    std::string bin_string = "";
    std::string tmp = "";
    std::string result = "";
    std::vector<std::string> six_bit_values;
    int count_24 = 0;

    for(char i: s)
        bin_string += std::bitset<8>(i).to_string();

    for(int i = 0; i < bin_string.length(); i++) {
        tmp += bin_string[i];

        if(tmp.length() == 6) {
            count_24 += 6;
            six_bit_values.push_back(tmp);
            tmp = "";
        }
    }

    if(tmp != "") {
        for(int i = 0; i <= 6 - tmp.length(); i++)
            tmp += "0";
        
        six_bit_values.push_back(tmp);
        count_24 += 6;
    }

    
    for(std::string six_bit_value: six_bit_values) {
        result += encode_table[binstoi(six_bit_value)];
    }

    if(count_24 % 24 != 0) {
        int diff = (24 - (count_24 % 24)) / 6;
        for(int i = 0; i < diff; i++) {
            result += "=";
        }
    }

    return result;
} 

std::string decode(std::string s) {
    std::vector<int> b64_dec;
    std::vector<std::string> eight_bit_values;
    std::string bin_str = "";
    std::string tmp = "";
    std::string result;

    for(char c: s) {
        for(int i = 0; i < 64; i++) {
            if(encode_table[i] == c) {
                b64_dec.push_back(i);
                break;
            }
        }
    }

    for(int i: b64_dec)
        bin_str += std::bitset<6>(i).to_string();

    for(int i = 0; i < bin_str.length(); i++) {
        tmp += bin_str[i];

        if(tmp.length() == 8) {
            eight_bit_values.push_back(tmp);
            tmp = "";
        }
    }

    for(std::string value: eight_bit_values) {
        result += (char)binstoi(value);
    }

    return result;
}